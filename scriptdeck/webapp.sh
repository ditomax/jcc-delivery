#!/bin/sh

### BEGIN INIT INFO
# Provides:          webapp
# Required-Start:    $local_fs $network
# Required-Stop:     $local_fs
# Default-Start:     2 3 4 5
# Default-Stop:      0 1 6
# Short-Description: webapp node.js
# Description:       webapp node.js
### END INIT INFO


# https://gist.github.com/jinze/3748766
# upgrade to this level

export PATH=$PATH:/usr/local/bin
export NODE_PATH=$NODE_PATH:/usr/local/lib/node_modules

case "$1" in
  start)
  exec forever --sourceDir=/var/www/JoReDa/webapp -a -l /var/log/JoReDa/webapp.log -p /var/log/JoReDa start server.js 10002 production
  ;;

  stop)
  exec forever stop --sourceDir=/var/www/JoReDa/webapp server.js
  ;;
esac

exit 0