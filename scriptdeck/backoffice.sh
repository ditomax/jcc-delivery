#!/bin/sh

### BEGIN INIT INFO
# Provides:          backoffice
# Required-Start:    $local_fs $network
# Required-Stop:     $local_fs
# Default-Start:     2 3 4 5
# Default-Stop:      0 1 6
# Short-Description: backoffice node.js
# Description:       backoffice node.js
### END INIT INFO

export PATH=$PATH:/usr/local/bin
export NODE_PATH=$NODE_PATH:/usr/local/lib/node_modules

case "$1" in
  start)
exec forever --sourceDir=/var/www/JoReDa/backoffice -a -l /var/log/JoReDa/backoffice.log -p /var/log/JoReDa start server.js 10001 production 
  ;;

  stop)
  exec forever stop --sourceDir=/var/www/JoReDa/backoffice server.js
  ;;
esac

exit 0
