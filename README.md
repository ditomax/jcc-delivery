# jcc-delivery

Auslieferungsrepository für das JCC help2day Projekt.


## Hintergrund




## Software Struktur

Das help2day Software Paket ist in zwei Teile unterteilt. Die **Webapp** ist das Frontend, das den Helfern die aktuellen Hilfsanfragen präsentiert. Das **Backend** ist das Verwaltungstool für die Hilfsorganisation die Hilfsanfragen erstellt und verwaltet.

Die Software basiert auf node.js einer Open Source Webserver Technologie auf Basis von Javascript. help2day verwendet die folgenden node.js Modulebibliotheken:

* express.js: Web Präsentation
* knex.js: Datenbankanschluß
* passport.js: Authentifikation
* sementic-ui: HTML Styling
* jquery.js: HTML Funktionen
* handlebars.js: Templating für emails und HTML Seiten 

Alle Abhängigkeiten sind in der Datei **package.json** pro Softwareteil definiert.


Beide Teile von help2day sind in node.js implementiert und in der folgenden Struktur im Filesystem gegliedert.

* app Verzeichnis: enthält alle relevanten Source Dateien für die Anwendungsfunktionen
* config Verzeichnis: enthält die Konfigurationsdateien und Initialisierungsfunktionen für Datenbank und Authentifizierung.
* public Verzeichnis: alle statischen Dateien für den Web Server.
* templates Verzeichnis: Vorlagen für emails
* views Verzeichnis: Vorlagen für dynamische HTML Seiten
* server.js Datei: Start und Initialisierung des Web Servers
* package.json Datei: Definition der Abhängigkeiten


## Installation



### Server Setup 

Die folgenden Setup Hinweise sind als Basisinformation zu sehen. Eine weiter Absicherung des Servers ist notwendig und nach den aktuellen Ständen der Sicherheitanforderungen auszulegen.

#### ssh daemon

* change port, allow only key based login

#### Apache

* Konfiguration (mod_ssl, proxy)
* https
* Zertifikate


#### mysql

* root user
* db user
* db struktur
* basiseinträge


#### Schlüsselfiles




#### Firewall

* block all inbound except, https, ssh 

#### fail2ban






### Transfer zum Server

### Update der Abhängigkeiten





## Software Entwicklung





## Autoren

* **Dietmar Millinger** - *Initial version of core modules in german* 

See also the list of [contributors](https://github.com/ditomaximal/help2day/contributors) who participated in this project.


## Lizenz

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details


## Danke

We use a lot of open source software in the project. It is impossible to list all of the contributors. But we want to **thank all of you**. 
Here is just a small list of components we use and love..

* node.js
* express.js
* moment.js
* knex.js
* crypto.js
* semantic-ui

