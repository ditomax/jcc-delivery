var config = {};

// PRODUCTION
config.own_customer_id= 2; 									// customer ID of operating organization. eg help2day, jcc
config.server_url= 'https://vps29299.alfahosting-vps.de';				// main url to backoffice web application
config.webapp='https://app.JoReDa.org';
config.image_url= config.server_url + '/image';
config.deeplink='/JoReDa';
config.deeplinkinstance='/hilfsanfrage';

// STAGE
//config.own_customer_id= 1;
//config.server_url= 'http://crm.likelynx.com';

// DEV
//config.own_customer_id= 1;
//config.server_url= 'http://localhost:8083';


// MYSQL database
config.database_user= 'joreda';
config.database_password= 'gVY3F074wOYtFgH';


// google api key for map
config.google_api_key= '***************';


// facebook
config.facebook_app_id= '***************';
config.facebook_secret= '***************';


// other configuration settings
config.GREX_CONFIG_REAGREE_BLOCK_SECONDS= 0;
config.strict_category= 'JoReDa';
config.pageSize= 40;

// main email account for sending emails
config.mail_user= 'confirmation@grex-app.com';
config.mail_password= '***************';

//email receiver configuration
config.internal_receivers= 'Swen.Maertin@jcc.johanniter.de';
config.internal_receivers_bcc= 'dietmar@grex-app.com';
//config.all_receivers= config.master_admin_receivers= 'dietmar@grex-app.com';
config.master_admin_receivers= 'Swen.Maertin@jcc.johanniter.de';
config.all_receivers= 'Swen.Maertin@jcc.johanniter.de';

// user ids with master admin rights
config.masterAdminId= 1;
config.masterAdminId1= 2;
config.masterAdminId2= 3;
config.masterAdminId3= 4;
config.masterAdminId4= 5;





module.exports = config;



