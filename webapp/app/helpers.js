/**
 * application logic of help2day APP
 * 
 * dietmar millinger @ grex it services gmbh
 * 
 */

/**
 * sets up required basic modules
 */
var crypto = require('crypto');
var path = require('path');
var mailer = require('nodemailer');
const Email = require('email-templates');
var config = require('../config/configuration.js');
var moment = require('moment');

var algorithm = 'aes-256-ctr';

var exports = module.exports = {};

exports.encrypt= function (email,salt,key) {
    var salt_sized = salt;
    var key_sized = key;
    
    var cipher = crypto.createCipheriv(algorithm, key_sized, salt_sized )
    var encrypted = cipher.update(email, 'utf8', 'hex')
    encrypted += cipher.final('hex');
    return encrypted;
};

exports.decrypt= function (encrypted,salt,key) {
    var decipher = crypto.createDecipheriv(algorithm, key, salt )
    var dec = decipher.update(encrypted, 'hex', 'utf8')
    dec += decipher.final('utf8');
    return dec;
};


exports.sendMailByTemplateAsync =  function ( data, template_name, receivers, bcc, subject ){
    
    var transporter = mailer.createTransport(
    {
        host : "smtp.gmail.com",
        secure : true,
        port: 465,
        auth: {
            user: config.mail_user,
            pass: config.mail_password
        }
    });

    const email = new Email({views: { options: { extension: 'handlebars' } }, transport: transporter, send: true } );

    var mail = 
    {
        from: "JoReDa <"+config.mail_user+">",
        to: receivers,
        bcc: bcc,
        subject: subject
    };
    
    email
    .send( { template: template_name, message: mail, locals: data })
    .then(res => {
        console.log('res.originalMessage', res.originalMessage)
    })
    .catch(console.error);
};



exports.getDistanceFromLatLonInKm = function (lat1,lon1,lat2,lon2) {
    var R = 6371; // Radius of the earth in km
    var dLat = deg2rad(lat2-lat1);  // deg2rad below
    var dLon = deg2rad(lon2-lon1); 
    var a = 
      Math.sin(dLat/2) * Math.sin(dLat/2) +
      Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) * 
      Math.sin(dLon/2) * Math.sin(dLon/2)
      ; 
    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a)); 
    var d = R * c; // Distance in km
    return d;
  };

function deg2rad(deg) {
    return deg * (Math.PI/180)
}



exports.incrementCounter = function ( db, key ){
	
	var id= -1;
	var today = moment().format("YYYY-MM-DD");

	db.knex('lm_counters')
	.where('co_day_string', '=', today )
	.then(function(data){

		if ( data.length < 1 ){

			var inserts = {}
			inserts['co_day_string'] = today;
			inserts[key]= 1;
			
			db.knex('lm_counters')
			.insert( inserts )
			.then(function(){return null;})
			
		}else{
			
			var counter= data[0];
			var updates= {}
			updates[key] = ( parseInt ( counter[key] ) || 0  ) + 1;

			//console.log("INFO: found record for update counter  " + key + ": " + JSON.stringify(counter) + " and updates " + JSON.stringify( updates ) );
			
			db.knex('lm_counters')
   	    	.where('co_id', '=', counter.co_id )
   	    	.update( updates )
			.then(function(){return null;})
		}
	});
	
}




