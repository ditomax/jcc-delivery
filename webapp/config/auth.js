var config= require('../config/configuration');
module.exports = {

    'facebookAuth' : {
        'clientID'      : config.facebook_app_id,   
        'clientSecret'  : config.facebook_secret,   
        'callbackURL'   : config.server_url + '/auth/facebook/clbk'
    }
};